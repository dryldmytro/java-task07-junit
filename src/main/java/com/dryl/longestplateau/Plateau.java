package com.dryl.longestplateau;

import java.util.List;
import java.util.Map;

public interface Plateau {
  Map<Integer, List<Integer>> getPlateaue(List<Integer> list);
}
