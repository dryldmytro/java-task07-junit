package com.dryl.longestplateau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlateauImpl implements Plateau{
  private List<List<Integer>> allPlateau;
  private List<Integer> plateauList;
  private int prev;
  private int now;

  public PlateauImpl() {
    allPlateau = new ArrayList<>();
    plateauList = new ArrayList<>();
  }

  public Map<Integer, List<Integer>> getPlateaue(List<Integer> list){
    Map<Integer,List<Integer>> map = new HashMap<>();
    for (int i = 0; i < list.size(); i++) {
      getIndexPlateau(list.get(i),i);
    }
    List<Integer>l = getBiggerPlateau();
    map.put(list.get(l.get(0)),l);
    return map;
  }
  private void getIndexPlateau(int i,int index){
    prev = now;
    now = i;
    if (prev==now){
      plateauList.add(index);
    }
    if (prev>now){
      allPlateau.add(plateauList);
      plateauList = new ArrayList<>();
    }
    if (prev<now){
      plateauList = new ArrayList<>();
      plateauList.add(index);
    }
  }
  private List<Integer> getBiggerPlateau(){
    if (!plateauList.isEmpty()){
      allPlateau.add(plateauList);
    }
    Collections.sort(allPlateau, Comparator.comparingInt(List::size));
    List<Integer> list = allPlateau.get(allPlateau.size()-1);
    System.out.println(list);
    return list;
  }
}
