package com.dryl;

public class Calculator {
  private CalcInterface calcInterface;

  public Calculator(CalcInterface calcInterface) {
    this.calcInterface = calcInterface;
  }
  public int calc(int a,int b){
    return calcInterface.add(a,b);
  }
}
