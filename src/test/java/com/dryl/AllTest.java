package com.dryl;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages("/Users/dmytrodrul/Downloads/junit/src/test/java/com/dryl/game")
public class AllTest {

}
