package com.dryl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.dryl.CalcInterface;
import com.dryl.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {
  @InjectMocks
  Calculator calculator;
  @Mock
  CalcInterface calcInterface;
  @Test
  public void testAdd(){
    when(calcInterface.add(10,20)).thenReturn(30);
    assertEquals(calculator.calc(10,20),30);
  }
}
