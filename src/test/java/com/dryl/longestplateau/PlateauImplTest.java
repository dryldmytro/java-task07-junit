package com.dryl.longestplateau;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
class PlateauImplTest {
  private PlateauImpl plateau = new PlateauImpl();
  static Stream<List<Integer>> arrayStream2() {
    List<List<Integer>> l = new ArrayList<>();
    List<Integer>list1 = Arrays.asList(1,2,2,2,3,3,3,2);
    List<Integer>list2 = Arrays.asList(2,3,4,5);
    List<Integer>list3 = Arrays.asList(1,1,1,0);
    List<Integer>list4 = Arrays.asList(1);
    List<Integer> list5 = Arrays.asList();
    l.add(list1);
    l.add(list2);
    l.add(list3);
    l.add(list4);
    l.add(list5);
    return l.stream();
  }

@ParameterizedTest
@MethodSource("arrayStream2")
void test(List<Integer> list){
    plateau.getPlateaue(list);
  Map<Integer,List<Integer>>m = plateau.getPlateaue(list);
  assertNotNull(m);
  }
  @RepeatedTest(5)
  void getPlateaue() {
    List<Integer> list = Arrays.asList(1,2,3,5,2,2,3,3);
    Map<Integer,List<Integer>>m = plateau.getPlateaue(list);
    Map<Integer, List<Integer>> map = new HashMap<>();
    map.put(3,Arrays.asList(6,7));
    assertEquals(map,m);
    assertNotNull(m);
  }
  @Test
  void testIntPrev() throws NoSuchFieldException {
    Field f = PlateauImpl.class.getDeclaredField("allPlateau");
    f.setAccessible(true);
    System.out.println(f.getType());
    assertNotNull(f);
  }
}